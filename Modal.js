/// <reference path="App.ts" />
var Tetris;
(function (Tetris) {
    var Modal = /** @class */ (function () {
        function Modal(app) {
            var _this = this;
            this.context = app.getContext().find('.modal');
            this.app = app;
            this.context.find('.close').click(function () {
                [_this.getBtnPlay(), _this.getBtnRestart()].some(function ($btn) {
                    if ($btn.filter(':visible').length > 0) {
                        if ($btn.is(_this.getBtnPlay())) {
                            if (_this.app.isAutoPlay()) {
                                $btn = _this.getBtnAutoPlay();
                            }
                        }
                        $btn.click();
                        return true;
                    }
                    return false;
                });
            });
            this.getBtnPlay().click(function () {
                _this.hide();
                _this.app.play(false);
            });
            this.getBtnAutoPlay().click(function () {
                _this.hide();
                _this.app.play(true);
            });
            this.getBtnRestart().click(function () {
                _this.hide();
                _this.app.startGame();
            });
            this.getBtnMute().click(function () {
                var audio = _this.app.getAudio();
                audio.setMute(!audio.isMute());
            });
            this.app.getContext().on(Tetris.AudioEvent.SetMute, function () {
                _this.updateBtnMute();
            });
            this.updateBtnMute();
        }
        Modal.prototype.updateBtnMute = function () {
            var $btnMute = this.getBtnMute();
            var audio = this.app.getAudio();
            $btnMute.text(((audio.isMute())
                ? $btnMute.text().replace(' off', ' on')
                : $btnMute.text().replace(' on', ' off')));
        };
        Modal.prototype.getBtnPlay = function () {
            return this.context.find('button.play');
        };
        Modal.prototype.getBtnAutoPlay = function () {
            return this.context.find('button.auto_play');
        };
        Modal.prototype.getBtnRestart = function () {
            return this.context.find('button.restart');
        };
        Modal.prototype.getBtnMute = function () {
            return this.context.find('button.mute');
        };
        Modal.prototype.showPause = function () {
            this.setMsg('Pause');
            this.getBtnPlay().removeClass('hide');
            this.getBtnAutoPlay().removeClass('hide');
            this.getBtnRestart().removeClass('hide');
            this.getBtnMute().removeClass('hide');
            this.show();
            var $btn_focus;
            if (this.app.isAutoPlay()) {
                $btn_focus = this.getBtnAutoPlay();
            }
            else {
                $btn_focus = this.getBtnPlay();
            }
            $btn_focus.focus();
        };
        Modal.prototype.showGameOver = function () {
            this.setMsg('Game Over');
            this.getBtnPlay().addClass('hide');
            this.getBtnAutoPlay().addClass('hide');
            this.getBtnRestart().removeClass('hide');
            this.getBtnMute().addClass('hide');
            this.show();
        };
        Modal.prototype.setMsg = function (msg) {
            this.context.find('.msg').removeClass('hide').text(msg);
        };
        Modal.prototype.show = function () {
            this.context.prev('.background_modal').removeClass('hide');
            this.context.removeClass('hide');
            var msg = this.context.find('.msg').text();
            $('title').text($('title').text().replace(/ \[.+]/, '') + ' [ ' + msg + ' ]');
        };
        Modal.prototype.hide = function () {
            this.context.prev('.background_modal').addClass('hide');
            this.context.addClass('hide');
            $('title').text($('title').text().replace(/ \[.+]/, '').trim());
        };
        return Modal;
    }());
    Tetris.Modal = Modal;
})(Tetris || (Tetris = {}));
