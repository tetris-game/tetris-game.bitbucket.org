/// <reference path="../../chalenkoa/www/assets/dep/_ide/jquery.d.ts" />

/// <reference path="App.ts" />
/// <reference path="Shape.ts" />

module Tetris
{
    export enum BoardEvent {
        CantDraw 			= 'BoardEventCantDraw',
		CantMove 			= 'BoardEventCantMove',
		RemoveFullLine 		= 'BoardEventRemoveFullLine',
    }

    export class Board
    {
		private context:JQuery;

		private width;
		private height;


        constructor ($context:JQuery, width:number, height:number)
		{
            this.context = $context;

            this.width = width;
            this.height = height;

            this.createAtoms();
        }


		public getContext()
		{
			return this.context;
		}

		public getWidth()
		{
			return this.width;
		}

		public getHeight()
		{
			return this.height;
		}


		public hideBorderEmpty()
		{
			this.context.find('ul').addClass('hide_border_empty');
		}


		public delete()
		{
			this.context.find('ul').remove();
		}


        public canDraw(shape)
		{
			let $atoms = this.getAtoms(shape);
			if ($atoms.length === 0) {
				return false;
			}

			let canDraw = true;

			$atoms.forEach(($atom) =>
			{
				if  ( ! canDraw) return;

				if ($atom.hasClass('shape')) {
					canDraw = false;
					return;
				}
			});

			return canDraw;
		}

        public drawShape(shape: Shape)
        {
			let canDraw = this.canDraw(shape);

            if ( ! canDraw) {
                this.context.trigger(BoardEvent.CantDraw);
                return false;
            }

			let $atoms = this.getAtoms(shape);
			if ($atoms.length === 0) {
				throw new Error('Error get atoms');
			}

            $atoms.forEach(($atom) =>
            {
                $atom
                    .addClass('shape')
                    .addClass(shape.getType());
            });

            return true;
        }


        public clearShape(shape: Shape)
        {
            let $atoms = this.getAtoms(shape);
            if ($atoms.length === 0) {
            	throw new Error('Error get atoms');
			}

            $atoms.forEach(($atom) =>
            {
                $atom
                    .removeClass('shape')
                    .removeClass(shape.getType());
            });
        }


        public getAtomsCantMove(shape: Shape, x = 0, y = 0, limit = 0)
		{
			let $atoms = this.getAtoms(shape);

			let counter = 0;

			let $atomsCantMove = $atoms.filter(($atom) =>
			{
				if (limit && counter >= limit) return false;

				let rowId = this.getRowIdForAtom($atom);
				let colId = this.getColIdForAtom($atom);

				rowId = rowId + x;
				colId = colId + y;

				let $atom_next = this.getAtom(rowId, colId);

				if ( $atom_next.length === 0) {
					counter ++;
					return true;
				}

				if ($atom_next.hasClass('shape'))
				{
					let $atom_in_shape = $atoms.filter(($atom:JQuery) =>
					{
						return $atom_next.get(0) === $atom.get(0) ? true : false;
					});

					if ($atom_in_shape.length === 0) {
						counter ++;
						return true;
					}
				}
			});

			return $atomsCantMove;
		}

        public canMove(shape: Shape, x = 0, y =0)
		{
            let $atoms = this.getAtoms(shape);
            if ($atoms.length === 0) {
            	return false;
			}

			let canMove = ( this.getAtomsCantMove(shape, x, y, 1).length > 0 ? false : true );

            return canMove;
		}

        public moveShape(shape: Shape, x = 0, y = 0)
        {
            if ( ! this.canMove(shape, x, y)) {
            	return false;
			}

            this.clearShape(shape);

            if (x !== 0) {
                shape.setRowId(shape.getRowId() + x);

            } else if (y !== 0) {
                shape.setColId(shape.getColId() + y);

            } else {
                throw new Error(`Wrong X = ${x} or Y = ${y}`)
            }

            this.drawShape(shape);

            return true;
        }


        public removeFullLines()
		{
			let rowIdsForDelete = [];

			for (let rowId = this.getHeight(); rowId >= 1; rowId--)
			{
				let atoms_shape = [];

				for (let colId = 1; colId <= this.getWidth(); colId++)
				{
					let $atom = this.getAtom(rowId, colId);

					if ( ! $atom.hasClass('shape')) break;

					atoms_shape.push($atom);
				}

				if (atoms_shape.length === this.getWidth())
				{
					rowIdsForDelete.push(rowId);
				}
			}

			rowIdsForDelete.forEach((rowId) =>
			{
				// remove line
				this.getAtom(rowId, 1).parent('li').remove();

				// add new line
				let $ul = $('ul', this.context);

				$ul.prepend("<li></li>");

				let liFirst = $("li:first", $ul);

				for (let colId = 1; colId <= this.getWidth(); colId++)
				{
					liFirst.append("<i class='atom'></i>");
				}

				this.context.trigger(Tetris.BoardEvent.RemoveFullLine);
			});
		}


		public removeEmptyLines()
		{
			let atoms_for_remove = [];

			['ver', 'hor'].forEach((direction) =>
			{
				[1,2,3,4].forEach((x) =>
				{
					let atoms_line = [];

					[1,2,3,4].forEach((y) =>
					{
						atoms_line.push(
							this.getAtom(
								direction == 'ver' ? y : x,
								direction == 'ver' ? x : y,
							)
						);
					});

					let atoms_shape = atoms_line.filter((atom:JQuery) =>
					{
						return atom.hasClass('shape') ? true : false;
					});

					if (atoms_shape.length === 0) {
						atoms_for_remove = atoms_for_remove.concat(atoms_line);
					}
				});
			});

			atoms_for_remove.forEach((atom:JQuery) =>
			{
				atom.remove();
			})
		}


		private createAtoms()
		{
			this.context.append("<ul class='atoms'></ul>");

			let $ul = $('ul', this.context);

			for(let rowId = 1; rowId <= this.getHeight(); rowId++)
			{
				$ul.append("<li></li>");

				let liLast = $("li:last", $ul);

				for(let colId = 1; colId <= this.getWidth(); colId++)
				{
					liLast.append("<i class='atom'></i>");
				}
			}

			this.context.css('min-height', $ul.height());
		}


		public cleanAtoms()
		{
			this.context.find('.atom').removeClass().addClass('atom');
		}


        private getAtoms(shape:Shape):JQuery[]
        {
            let data = shape.getData();

            let $atoms = [];

            for(let rowIdShape = 1; rowIdShape <= 4; rowIdShape ++)
            {
                for (let colIdShape = 1; colIdShape <= 4; colIdShape ++)
                {
					let shapeAtom = data[rowIdShape - 1][colIdShape - 1];

                    if (shapeAtom != 1) continue;

					let shapeRowIdOnBoard = shape.getRowId();
					let shapeColIdOnBoard = shape.getColId();

					let rowIdBoard = shapeRowIdOnBoard - (rowIdShape- 1);
					let colIdBoard = shapeColIdOnBoard + (colIdShape - 1);

					let $atom = this.getAtom(rowIdBoard, colIdBoard);
					if ($atom.length === 0) {
						return [];
					}

                    $atoms.push($atom);
                }
            }
            return $atoms;
        }


        public getAtom(rowId, colId):JQuery
        {
        	if (colId < 1) return $();
			if (colId > this.getWidth()) return $();

            return $('li:eq('+(this.getHeight() - rowId)+') .atom:eq('+(colId - 1)+')', this.context);
        }


        public getColIdForAtom($atom: JQuery)
        {
            return $atom.index() + 1;
        }

        public getRowIdForAtom($atom)
        {
            return this.getHeight() - $atom.parents('li').index();
        }
    }
}