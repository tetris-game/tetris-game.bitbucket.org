/// <reference path="../../chalenkoa/www/assets/dep/_ide/jquery.d.ts" />

/// <reference path="App.ts" />

module Tetris
{
    export class AppEventManager
    {
		static KEY_CODES_PAUSE 	    = [19, 27]; // pause, esc
		static KEY_CODES_GO_LEFT 	= [37]; // left
		static KEY_CODES_GO_RIGHT 	= [39]; // right
		static KEY_CODES_GO_DOWN	= [40]; // down
		static KEY_CODES_ROTATE 	= [38, 32]; // up, space

        private app: Tetris.App;

		private idCurrentShape;

		private timeLastKeyDown;


        constructor(app: Tetris.App)
        {
            this.app = app;

            $(window).on(
                'keydown',
				(e) =>
				{
					if (this.app.isGameOver()) return;

                	if (Tetris.AppEventManager.KEY_CODES_PAUSE.indexOf(e.which) > -1)
                	{
                		if (this.app.isPause())
                		{
                			this.app.play();

						} else {
                			this.app.pause();
						}
					}

					if (this.app.isPause()) return;

					if ( ! this.idCurrentShape) {
						this.idCurrentShape = this.app.getCurrentShape().getId();
					}

					// предотвращаем срабатывания нажатия нажатого еще для прошлой фигуры
					// для следующей фигуры необходимо отжат кнопку и нажать снова
					if (this.idCurrentShape !== this.app.getCurrentShape().getId()) return;

					this.onKeyDown(e);
				}
            );

			$(window).on(
				'keyup',
				(e) => {
					if (this.app.isPause()) return;

					delete(this.idCurrentShape);
				}
			);
        }

        private onKeyDown(e)
        {
            let key_code = e.which;

            let timeNow = (new Date()).getTime() / 1000;

            let fastPress = false;

            if (this.timeLastKeyDown) {
            	let intervalSec = (timeNow - this.timeLastKeyDown);

				if (intervalSec < 0.1) {
					fastPress = true;
				}
			}

            if (AppEventManager.KEY_CODES_ROTATE.indexOf(key_code) > -1)
            {
                this.onRotate();

            } else if (AppEventManager.KEY_CODES_GO_LEFT.indexOf(key_code) > -1)
            {
                this.onGoLeft();

            } else if (AppEventManager.KEY_CODES_GO_RIGHT.indexOf(key_code) > -1)
            {
                this.onGoRight();

            } else if (AppEventManager.KEY_CODES_GO_DOWN.indexOf(key_code) > -1)
			{
				this.onGoDown(fastPress);
			}

			this.timeLastKeyDown = timeNow;
        }

        public onRotate()
        {
        	let currentShape = this.app.getCurrentShape();
			let board 		 = this.app.getBoard();

			let draw = false;

			board.clearShape(currentShape);

			currentShape.rotate();

			let offsets = [0, 1, -1, 2, -2].forEach((offset) =>
			{
				if (draw) return;

				if (offset) {
					currentShape.setColId(currentShape.getColId() + offset);
				}

				if ( ! board.canDraw(currentShape))
				{
					currentShape.setColId(currentShape.getColId()- offset);

					return;
				}

				board.drawShape(currentShape);

				draw = true;
			});

			if ( ! draw)
			{
				currentShape.rotateReverse();

				board.drawShape(currentShape);
			}
        }

        private onGoLeft()
        {
			this.app.getBoard().moveShape(this.app.getCurrentShape(), 0, -1);
        }

        private onGoRight()
        {
			this.app.getBoard().moveShape(this.app.getCurrentShape(), 0, 1);
        }

        private onGoDown(fast = false)
		{
			let x = -1;

			// При быстром движении за раз перескакиваем на 2 клетки но нужно убедиться что на одну клетку так же
			// можно перейти так как там может быть какой то препятские
			if (
					fast
				&& 	this.app.getBoard().canMove(this.app.getCurrentShape(), -2)
				&& 	this.app.getBoard().canMove(this.app.getCurrentShape(), -1)
			) {
				x = -2;
			}

			let moveResult = this.app.getBoard().moveShape(
				this.app.getCurrentShape(),
				x
			);

			if ( ! moveResult && Math.abs(x) > 1)
			{
				x = -1;

				moveResult = this.app.getBoard().moveShape(
					this.app.getCurrentShape(),
					x
				);
			}

			if ( ! moveResult)
			{
				let $atomsCantMove = this.app.getBoard().getAtomsCantMove(
					this.app.getCurrentShape(),
					x
				);

				this.app.getBoard().getContext().trigger(Tetris.BoardEvent.CantMove, [$atomsCantMove]);
			}
		}
    }
}