
/// <reference path="App.ts" />

module Tetris
{
	export class BtnPause
	{
		private context:JQuery;

		private app: App;


		constructor (app: Tetris.App)
		{
			this.app = app;

			this.context = this.app.getContext().find('.toolbar .btns button.pause');

			$(window).blur(() =>
			{
				if (this.isPlay()) {
					this.pause();
				}
			});

			this.context.click(() =>
			{
				if (this.isPlay())
				{
					this.pause()

				} else {
					this.play();
				}
			});

			// Убираем фокус с кнопки, чтобы по нажатию пробела не было нажатия на эту кноку
			this.context.on('focus', () =>
			{
				this.context[0].blur();
			});
		}


		public pause(only_state = false)
		{
			if (this.isPause()) return;

			this.context.addClass('active');

			if ( ! only_state ) {
				this.app.pause();
			}
		}

		public play(only_state = false)
		{
			if (this.isPlay()) return;

			this.context.removeClass('active');

			if ( ! only_state ) {
				this.app.play();
			}
		}


		public isPause()
		{
			return this.context.hasClass('active');
		}

		public isPlay()
		{
			return ! this.context.hasClass('active');
		}
	}
}