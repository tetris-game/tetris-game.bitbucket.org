var Tetris;
(function (Tetris) {
    var AudioEvent;
    (function (AudioEvent) {
        AudioEvent["SetMute"] = "AudioEventSetMute";
    })(AudioEvent = Tetris.AudioEvent || (Tetris.AudioEvent = {}));
    var Audio = /** @class */ (function () {
        function Audio(app) {
            this.app = app;
            this.setMute(localStorage.getItem('tetris_audio_mute'));
        }
        Audio.prototype.setMute = function (value) {
            this.mute = value > 0 ? 1 : 0;
            localStorage.setItem('tetris_audio_mute', '' + this.mute);
            this.app.getContext().trigger(Tetris.AudioEvent.SetMute);
        };
        Audio.prototype.isMute = function () {
            return this.mute > 0 ? true : false;
        };
        Audio.prototype.play = function (audio_id) {
            if (this.isMute())
                return;
            var audio = $('#' + audio_id)[0];
            // @see https://stackoverflow.com/questions/36803176/how-to-prevent-the-play-request-was-interrupted-by-a-call-to-pause-error
            var isPlaying = audio.currentTime > 0 && !audio.paused && !audio.ended && audio.readyState > 2;
            if (isPlaying) {
                audio.pause();
            }
            audio.currentTime = 0;
            audio.play();
        };
        Audio.AUDIO_ATOM_STOP = 'audio_atom_stop';
        Audio.AUDIO_LEVEL_UP = 'audio_level_up';
        Audio.AUDIO_LINE_DELETE = 'audio_line_delete';
        return Audio;
    }());
    Tetris.Audio = Audio;
})(Tetris || (Tetris = {}));
