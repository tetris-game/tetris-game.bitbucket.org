var Tetris;
(function (Tetris) {
    var AutoPlayerWatcher = /** @class */ (function () {
        function AutoPlayerWatcher(app) {
            var _this = this;
            this.app = app;
            var $modal_msg = this.app.getContext().find('.modal .msg');
            var modal_msg_text_back;
            $($modal_msg).bind("DOMSubtreeModified", function () {
                if ($modal_msg.text() !== modal_msg_text_back && $modal_msg.text().toLowerCase() === 'game over') {
                    _this.gameOver();
                }
                ;
                modal_msg_text_back = $modal_msg.text();
            });
        }
        AutoPlayerWatcher.prototype.gameOver = function () {
            var score = $('.toolbar .score .value').text();
            console.log("game over [score = " + score + "]");
            setTimeout(function () {
                $('.modal button.restart').click();
            }, 0);
        };
        return AutoPlayerWatcher;
    }());
    Tetris.AutoPlayerWatcher = AutoPlayerWatcher;
})(Tetris || (Tetris = {}));
