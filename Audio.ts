module Tetris
{
	export enum AudioEvent {
		SetMute = 'AudioEventSetMute'
	}

	export class Audio
	{
		public static AUDIO_ATOM_STOP = 'audio_atom_stop';
		public static AUDIO_LEVEL_UP = 'audio_level_up';
		public static AUDIO_LINE_DELETE = 'audio_line_delete';

		private app: App;
		private mute;


		constructor(app: App)
		{
			this.app = app;

			this.setMute( localStorage.getItem('tetris_audio_mute') );
		}


		public setMute(value)
		{
			this.mute = value > 0 ? 1 : 0;

			localStorage.setItem('tetris_audio_mute', ''+this.mute);

			this.app.getContext().trigger(Tetris.AudioEvent.SetMute);
		}

		public isMute()
		{
			return this.mute > 0 ? true : false;
		}


		public play(audio_id)
		{
			if (this.isMute()) return;

			let audio:HTMLAudioElement = $('#'+audio_id)[0];

			// @see https://stackoverflow.com/questions/36803176/how-to-prevent-the-play-request-was-interrupted-by-a-call-to-pause-error
			let isPlaying = audio.currentTime > 0 && ! audio.paused && ! audio.ended && audio.readyState > 2;

			if (isPlaying) {
				audio.pause();
			}

			audio.currentTime = 0;

			audio.play();
		}
	}
}