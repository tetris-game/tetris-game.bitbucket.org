/// <reference path="../../chalenkoa/www/assets/dep/_ide/jquery.d.ts" />
/// <reference path="App.ts" />
/// <reference path="Shape.ts" />
var Tetris;
(function (Tetris) {
    var BoardEvent;
    (function (BoardEvent) {
        BoardEvent["CantDraw"] = "BoardEventCantDraw";
        BoardEvent["CantMove"] = "BoardEventCantMove";
        BoardEvent["RemoveFullLine"] = "BoardEventRemoveFullLine";
    })(BoardEvent = Tetris.BoardEvent || (Tetris.BoardEvent = {}));
    var Board = /** @class */ (function () {
        function Board($context, width, height) {
            this.context = $context;
            this.width = width;
            this.height = height;
            this.createAtoms();
        }
        Board.prototype.getContext = function () {
            return this.context;
        };
        Board.prototype.getWidth = function () {
            return this.width;
        };
        Board.prototype.getHeight = function () {
            return this.height;
        };
        Board.prototype.hideBorderEmpty = function () {
            this.context.find('ul').addClass('hide_border_empty');
        };
        Board.prototype.delete = function () {
            this.context.find('ul').remove();
        };
        Board.prototype.canDraw = function (shape) {
            var $atoms = this.getAtoms(shape);
            if ($atoms.length === 0) {
                return false;
            }
            var canDraw = true;
            $atoms.forEach(function ($atom) {
                if (!canDraw)
                    return;
                if ($atom.hasClass('shape')) {
                    canDraw = false;
                    return;
                }
            });
            return canDraw;
        };
        Board.prototype.drawShape = function (shape) {
            var canDraw = this.canDraw(shape);
            if (!canDraw) {
                this.context.trigger(BoardEvent.CantDraw);
                return false;
            }
            var $atoms = this.getAtoms(shape);
            if ($atoms.length === 0) {
                throw new Error('Error get atoms');
            }
            $atoms.forEach(function ($atom) {
                $atom
                    .addClass('shape')
                    .addClass(shape.getType());
            });
            return true;
        };
        Board.prototype.clearShape = function (shape) {
            var $atoms = this.getAtoms(shape);
            if ($atoms.length === 0) {
                throw new Error('Error get atoms');
            }
            $atoms.forEach(function ($atom) {
                $atom
                    .removeClass('shape')
                    .removeClass(shape.getType());
            });
        };
        Board.prototype.getAtomsCantMove = function (shape, x, y, limit) {
            var _this = this;
            if (x === void 0) { x = 0; }
            if (y === void 0) { y = 0; }
            if (limit === void 0) { limit = 0; }
            var $atoms = this.getAtoms(shape);
            var counter = 0;
            var $atomsCantMove = $atoms.filter(function ($atom) {
                if (limit && counter >= limit)
                    return false;
                var rowId = _this.getRowIdForAtom($atom);
                var colId = _this.getColIdForAtom($atom);
                rowId = rowId + x;
                colId = colId + y;
                var $atom_next = _this.getAtom(rowId, colId);
                if ($atom_next.length === 0) {
                    counter++;
                    return true;
                }
                if ($atom_next.hasClass('shape')) {
                    var $atom_in_shape = $atoms.filter(function ($atom) {
                        return $atom_next.get(0) === $atom.get(0) ? true : false;
                    });
                    if ($atom_in_shape.length === 0) {
                        counter++;
                        return true;
                    }
                }
            });
            return $atomsCantMove;
        };
        Board.prototype.canMove = function (shape, x, y) {
            if (x === void 0) { x = 0; }
            if (y === void 0) { y = 0; }
            var $atoms = this.getAtoms(shape);
            if ($atoms.length === 0) {
                return false;
            }
            var canMove = (this.getAtomsCantMove(shape, x, y, 1).length > 0 ? false : true);
            return canMove;
        };
        Board.prototype.moveShape = function (shape, x, y) {
            if (x === void 0) { x = 0; }
            if (y === void 0) { y = 0; }
            if (!this.canMove(shape, x, y)) {
                return false;
            }
            this.clearShape(shape);
            if (x !== 0) {
                shape.setRowId(shape.getRowId() + x);
            }
            else if (y !== 0) {
                shape.setColId(shape.getColId() + y);
            }
            else {
                throw new Error("Wrong X = " + x + " or Y = " + y);
            }
            this.drawShape(shape);
            return true;
        };
        Board.prototype.removeFullLines = function () {
            var _this = this;
            var rowIdsForDelete = [];
            for (var rowId = this.getHeight(); rowId >= 1; rowId--) {
                var atoms_shape = [];
                for (var colId = 1; colId <= this.getWidth(); colId++) {
                    var $atom = this.getAtom(rowId, colId);
                    if (!$atom.hasClass('shape'))
                        break;
                    atoms_shape.push($atom);
                }
                if (atoms_shape.length === this.getWidth()) {
                    rowIdsForDelete.push(rowId);
                }
            }
            rowIdsForDelete.forEach(function (rowId) {
                // remove line
                _this.getAtom(rowId, 1).parent('li').remove();
                // add new line
                var $ul = $('ul', _this.context);
                $ul.prepend("<li></li>");
                var liFirst = $("li:first", $ul);
                for (var colId = 1; colId <= _this.getWidth(); colId++) {
                    liFirst.append("<i class='atom'></i>");
                }
                _this.context.trigger(Tetris.BoardEvent.RemoveFullLine);
            });
        };
        Board.prototype.removeEmptyLines = function () {
            var _this = this;
            var atoms_for_remove = [];
            ['ver', 'hor'].forEach(function (direction) {
                [1, 2, 3, 4].forEach(function (x) {
                    var atoms_line = [];
                    [1, 2, 3, 4].forEach(function (y) {
                        atoms_line.push(_this.getAtom(direction == 'ver' ? y : x, direction == 'ver' ? x : y));
                    });
                    var atoms_shape = atoms_line.filter(function (atom) {
                        return atom.hasClass('shape') ? true : false;
                    });
                    if (atoms_shape.length === 0) {
                        atoms_for_remove = atoms_for_remove.concat(atoms_line);
                    }
                });
            });
            atoms_for_remove.forEach(function (atom) {
                atom.remove();
            });
        };
        Board.prototype.createAtoms = function () {
            this.context.append("<ul class='atoms'></ul>");
            var $ul = $('ul', this.context);
            for (var rowId = 1; rowId <= this.getHeight(); rowId++) {
                $ul.append("<li></li>");
                var liLast = $("li:last", $ul);
                for (var colId = 1; colId <= this.getWidth(); colId++) {
                    liLast.append("<i class='atom'></i>");
                }
            }
            this.context.css('min-height', $ul.height());
        };
        Board.prototype.cleanAtoms = function () {
            this.context.find('.atom').removeClass().addClass('atom');
        };
        Board.prototype.getAtoms = function (shape) {
            var data = shape.getData();
            var $atoms = [];
            for (var rowIdShape = 1; rowIdShape <= 4; rowIdShape++) {
                for (var colIdShape = 1; colIdShape <= 4; colIdShape++) {
                    var shapeAtom = data[rowIdShape - 1][colIdShape - 1];
                    if (shapeAtom != 1)
                        continue;
                    var shapeRowIdOnBoard = shape.getRowId();
                    var shapeColIdOnBoard = shape.getColId();
                    var rowIdBoard = shapeRowIdOnBoard - (rowIdShape - 1);
                    var colIdBoard = shapeColIdOnBoard + (colIdShape - 1);
                    var $atom = this.getAtom(rowIdBoard, colIdBoard);
                    if ($atom.length === 0) {
                        return [];
                    }
                    $atoms.push($atom);
                }
            }
            return $atoms;
        };
        Board.prototype.getAtom = function (rowId, colId) {
            if (colId < 1)
                return $();
            if (colId > this.getWidth())
                return $();
            return $('li:eq(' + (this.getHeight() - rowId) + ') .atom:eq(' + (colId - 1) + ')', this.context);
        };
        Board.prototype.getColIdForAtom = function ($atom) {
            return $atom.index() + 1;
        };
        Board.prototype.getRowIdForAtom = function ($atom) {
            return this.getHeight() - $atom.parents('li').index();
        };
        return Board;
    }());
    Tetris.Board = Board;
})(Tetris || (Tetris = {}));
