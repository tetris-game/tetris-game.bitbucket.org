/// <reference path="App.ts" />
var Tetris;
(function (Tetris) {
    var AutoPlayer = /** @class */ (function () {
        function AutoPlayer(app) {
            var _this = this;
            this.app = app;
            this.app.getContext().on(Tetris.AppEvent.AddShapeOnBoard, function () {
                _this.run();
            });
            var auto_player_watcher = this.app.getContext().data('auto_player_watcher');
            if (!auto_player_watcher) {
                this.app.getContext().data('auto_player_watcher', new Tetris.AutoPlayerWatcher(this.app));
            }
        }
        AutoPlayer.prototype.stop = function () {
            this.app.getContext().off(Tetris.AppEvent.AddShapeOnBoard);
        };
        AutoPlayer.prototype.run = function () {
            var _this = this;
            var variants = [];
            // Получаем карту поверхности на доске
            var mapUpBord = this.getRowIsUpForCol();
            // Детектим дыры
            //--------------
            var minDepthHole = 3;
            var colIdHole = null;
            var minRowIdHole = null;
            mapUpBord.forEach(function (rowId, colId) {
                if ((colId === 1
                    && (mapUpBord[colId + 1] - rowId) >= minDepthHole)
                    || ((mapUpBord[colId + 1] - rowId) >= minDepthHole
                        && (mapUpBord[colId - 1] - rowId) >= minDepthHole)
                    || (colId === _this.app.getBoard().getWidth()
                        && (mapUpBord[colId - 1] - rowId) >= minDepthHole)) {
                    if (!colIdHole
                        || minRowIdHole > rowId) {
                        colIdHole = colId;
                        minRowIdHole = rowId;
                    }
                }
            });
            //--------------
            [1, 2, 3, 4].forEach(function () {
                _this.app.getCurrentShape().rotate();
                // Получаем карту дна текущей фигуры
                //----------------------------------
                /** @see https://stackoverflow.com/questions/7848004/get-column-from-a-two-dimensional-array-in-javascript */
                var arrayColumn = function (arr, n) { return arr.map(function (x) { return x[n]; }); };
                var shape = _this.app.getCurrentShape();
                var shapeData = shape.getData();
                var mapDownShape = [];
                for (var colId = 0; colId < 4; colId++) {
                    var rowIds = arrayColumn(shapeData, colId);
                    var rowId = rowIds.lastIndexOf(1);
                    mapDownShape[colId] = rowId;
                }
                //----------------------------------
                // Ищем совпадения
                //----------------
                var offsetColId = mapDownShape.findIndex(function (colId) {
                    return colId > -1;
                });
                var mapDownShapeOnlyShape = mapDownShape.filter(function (colId) {
                    return colId === -1 ? false : true;
                });
                mapUpBord.forEach(function (rowIdBoard, colIdBoard, mapUpBord) {
                    var sumColIds = mapDownShapeOnlyShape.map(function (rowIdShape, colIdShape) {
                        return rowIdShape + mapUpBord[colIdShape + colIdBoard];
                    });
                    /** @see https://stackoverflow.com/questions/1669190/find-the-min-max-element-of-an-array-in-javascript */
                    var maxSum = Math.max.apply(null, sumColIds);
                    var diffsSumColIds = sumColIds.map(function (sum) {
                        return Math.abs(sum - maxSum);
                    });
                    var emptyAtom = diffsSumColIds.reduce(function (a, b) {
                        return a + b;
                    });
                    if (isNaN(emptyAtom))
                        return;
                    var emptyAtomColIds = {};
                    diffsSumColIds.forEach(function (emptyAtom, colId) {
                        emptyAtomColIds[colId + colIdBoard] = emptyAtom;
                    });
                    variants.push({
                        shapeState: shape.getState(),
                        colIdBoard: colIdBoard - offsetColId,
                        rowIdBoard: rowIdBoard,
                        emptyAtom: emptyAtom,
                        emptyAtomColIds: emptyAtomColIds
                    });
                });
            });
            if (colIdHole) {
                variants = variants.filter(function (variant) {
                    return (Object.keys(variant.emptyAtomColIds).indexOf('' + colIdHole) > -1
                        ? true
                        : false);
                });
            }
            variants.sort(function (variant_a, variant_b) {
                var weightEmptyAtom = 100;
                var weightRowId = 1;
                var a = (variant_a.emptyAtom * weightEmptyAtom) + (variant_a.rowIdBoard * weightRowId);
                var b = (variant_b.emptyAtom * weightEmptyAtom) + (variant_b.rowIdBoard * weightRowId);
                if (a < b) {
                    return -1;
                }
                if (a > b) {
                    return 1;
                }
                return 0;
            });
            var variantBest = variants[0];
            this.applyVariant(variantBest);
        };
        AutoPlayer.prototype.applyVariant = function (variant) {
            var _this = this;
            setTimeout(function () {
                var shape = _this.app.getCurrentShape();
                [1, 2, 3, 4].forEach(function () {
                    if (variant.shapeState === shape.getState())
                        return;
                    jQuery.event.trigger({ type: 'keydown', which: Tetris.AppEventManager.KEY_CODES_ROTATE[0] });
                });
                var diffColId = variant.colIdBoard - shape.getColId();
                var moveDown = function () {
                    var idIntervalDown = setInterval(function () {
                        if (_this.app.isPause()) {
                            clearInterval(idIntervalDown);
                            return;
                        }
                        var canMove = _this.app.getBoard().canMove(shape, -1);
                        if (canMove) {
                            jQuery.event.trigger({ type: 'keydown', which: Tetris.AppEventManager.KEY_CODES_GO_DOWN[0] });
                        }
                        if (!canMove) {
                            clearInterval(idIntervalDown);
                            jQuery.event.trigger({ type: 'keyup' });
                            return;
                        }
                    }, 1000 / 100);
                };
                if (diffColId === 0) {
                    moveDown();
                }
                else {
                    var which_1 = ((diffColId < 0)
                        ? Tetris.AppEventManager.KEY_CODES_GO_LEFT[0]
                        : Tetris.AppEventManager.KEY_CODES_GO_RIGHT[0]);
                    var counter_1 = 0;
                    var idIntervalSide_1 = setInterval(function () {
                        if (counter_1 >= Math.abs(diffColId)) {
                            clearInterval(idIntervalSide_1);
                            jQuery.event.trigger({ type: 'keyup' });
                            moveDown();
                            return;
                        }
                        counter_1++;
                        jQuery.event.trigger({ type: 'keydown', which: which_1 });
                    }, 1000 / 30);
                }
            }, 500);
        };
        AutoPlayer.prototype.getRowIsUpForCol = function () {
            var _this = this;
            // [col_id] => [row_id_1, row_id_2, ...]
            var rowIdsForCol = [];
            var atomsAdded = [];
            for (var colId = 1; colId <= this.app.getBoard().getWidth(); colId++) {
                rowIdsForCol[colId] = [0];
            }
            var board = this.app.getBoard();
            var $atomsFirstRow = $('.board .atoms li:last .atom.shape');
            if ($atomsFirstRow.length > 0) {
                var addAtoms_1 = function (atoms) {
                    atoms = atoms.filter(function ($atom) {
                        if (atomsAdded.indexOf($atom[0]) > -1)
                            return false;
                        atomsAdded.push($atom[0]);
                        return true;
                    });
                    atoms.forEach(function ($atom) {
                        var colId = board.getColIdForAtom($atom);
                        var rowId = board.getRowIdForAtom($atom);
                        if (!rowIdsForCol[colId]) {
                            rowIdsForCol[colId] = [];
                        }
                        if (rowIdsForCol[colId].indexOf(rowId) === -1) {
                            rowIdsForCol[colId].push(rowId);
                        }
                    });
                    return atoms;
                };
                var addAtomsForEach_1 = function (atoms) {
                    atoms = addAtoms_1(atoms);
                    if (atoms.length === 0)
                        return;
                    atoms.forEach(function ($atom) {
                        atoms = _this.getAtomsShapeAroundAtom($atom);
                        addAtomsForEach_1(atoms);
                    });
                };
                $atomsFirstRow.each(function (index, elem) {
                    addAtomsForEach_1([$(elem)]);
                });
            }
            for (var colId in rowIdsForCol) {
                var rowIds = rowIdsForCol[colId];
                var rowIdMax = Math.max.apply(null, rowIds);
                rowIdsForCol[colId] = rowIdMax;
            }
            return rowIdsForCol;
        };
        AutoPlayer.prototype.getAtomsShapeAroundAtom = function ($atom) {
            var board = this.app.getBoard();
            var rowId = board.getRowIdForAtom($atom);
            var cowId = board.getColIdForAtom($atom);
            var atoms = [];
            atoms.push(board.getAtom(rowId, cowId - 1));
            atoms.push(board.getAtom(rowId + 1, cowId - 1));
            atoms.push(board.getAtom(rowId + 1, cowId));
            atoms.push(board.getAtom(rowId + 1, cowId + 1));
            atoms.push(board.getAtom(rowId, cowId + 1));
            atoms = atoms.filter(function ($atom) {
                return $atom.hasClass('shape') ? true : false;
            });
            return atoms;
        };
        return AutoPlayer;
    }());
    Tetris.AutoPlayer = AutoPlayer;
})(Tetris || (Tetris = {}));
