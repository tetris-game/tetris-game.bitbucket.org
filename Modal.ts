/// <reference path="App.ts" />

module Tetris
{
	export class Modal
	{
		private context:JQuery;

		private app: App;


		constructor(app: App)
		{
			this.context = app.getContext().find('.modal');

			this.app = app;

			this.context.find('.close').click(() =>
			{
				[this.getBtnPlay(), this.getBtnRestart()].some(($btn) =>
				{
					if ($btn.filter(':visible').length > 0)
					{
						if ($btn.is(this.getBtnPlay()))
						{
							if (this.app.isAutoPlay()) {
								$btn = this.getBtnAutoPlay();
							}
						}

						$btn.click();

						return true;
					}
					return false;
				});
			});

			this.getBtnPlay().click(() =>
			{
				this.hide();

				this.app.play( false);
			});

			this.getBtnAutoPlay().click(() =>
			{
				this.hide();

				this.app.play(true);
			});

			this.getBtnRestart().click(() =>
			{
				this.hide();

				this.app.startGame();
			});

			this.getBtnMute().click(() =>
			{
				let audio: Tetris.Audio = this.app.getAudio();

				audio.setMute( ! audio.isMute());
			});

			this.app.getContext().on(Tetris.AudioEvent.SetMute, () =>
			{
				this.updateBtnMute();
			});

			this.updateBtnMute();
		}


		public updateBtnMute()
		{
			let $btnMute = this.getBtnMute();
			let audio: Tetris.Audio = this.app.getAudio();

			$btnMute.text(
				(
					( audio.isMute() )
					? $btnMute.text().replace(' off', ' on')
					: $btnMute.text().replace(' on', ' off')
				)
			);
		}


		private getBtnPlay()
		{
			return this.context.find('button.play');
		}

		private getBtnAutoPlay()
		{
			return this.context.find('button.auto_play');
		}

		private getBtnRestart()
		{
			return this.context.find('button.restart');
		}

		private getBtnMute()
		{
			return this.context.find('button.mute');
		}


		public showPause()
		{
			this.setMsg('Pause');

			this.getBtnPlay().removeClass('hide');
			this.getBtnAutoPlay().removeClass('hide');
			this.getBtnRestart().removeClass('hide');
			this.getBtnMute().removeClass('hide');

			this.show();

			let $btn_focus: JQuery;

			if (this.app.isAutoPlay()) {
				$btn_focus = this.getBtnAutoPlay()
			} else {
				$btn_focus = this.getBtnPlay();
			}

			$btn_focus.focus();
		}

		public showGameOver()
		{
			this.setMsg('Game Over');

			this.getBtnPlay().addClass('hide');
			this.getBtnAutoPlay().addClass('hide');
			this.getBtnRestart().removeClass('hide');
			this.getBtnMute().addClass('hide');

			this.show();
		}


		private setMsg(msg)
		{
			this.context.find('.msg').removeClass('hide').text(msg);
		}

		private show()
		{
			this.context.prev('.background_modal').removeClass('hide');

			this.context.removeClass('hide');

			let msg = this.context.find('.msg').text();

			$('title').text(
				$('title').text().replace(/ \[.+]/, '')+' [ '+msg+' ]'
			);
		}

		public hide()
		{
			this.context.prev('.background_modal').addClass('hide');

			this.context.addClass('hide');

			$('title').text(
				$('title').text().replace(/ \[.+]/, '').trim()
			);
		}
	}
}