/// <reference path="ShapeData.ts" />
var Tetris;
(function (Tetris) {
    var ShapeType;
    (function (ShapeType) {
        ShapeType["Line"] = "Line";
        ShapeType["Square"] = "Square";
        ShapeType["LShape"] = "LShape";
        ShapeType["JShape"] = "JShape";
        ShapeType["Tree"] = "Tree";
        ShapeType["ZShape"] = "ZShape";
        ShapeType["SShape"] = "SShape";
    })(ShapeType = Tetris.ShapeType || (Tetris.ShapeType = {}));
    var Shape = /** @class */ (function () {
        function Shape(type, rowIdCurrent, colIdCurrent) {
            if (rowIdCurrent === void 0) { rowIdCurrent = 0; }
            if (colIdCurrent === void 0) { colIdCurrent = 0; }
            this.id = Math.random();
            this.type = type;
            this.data = Tetris.ShapeData.DATA[this.type];
            this.state = 0;
        }
        Shape.prototype.getId = function () {
            return this.id;
        };
        Shape.prototype.setRowId = function (rowId) {
            this.rowIdCurrent = rowId;
        };
        Shape.prototype.getRowId = function () {
            return this.rowIdCurrent;
        };
        Shape.prototype.setColId = function (colId) {
            this.colIdCurrent = colId;
        };
        Shape.prototype.getColId = function () {
            return this.colIdCurrent;
        };
        Shape.prototype.getType = function () {
            return this.type;
        };
        /**
         * @returns []
         */
        Shape.prototype.getData = function () {
            return this.data[this.state];
        };
        /**
         * Вращение по часовой
         */
        Shape.prototype.rotate = function () {
            this._rotate(1);
        };
        /**
         * Вращение против часовой
         */
        Shape.prototype.rotateReverse = function () {
            this._rotate(-1);
        };
        Shape.prototype._rotate = function (state_offset) {
            if (state_offset === void 0) { state_offset = 0; }
            this.setState(this.state + state_offset);
            if (this.state > 3) {
                this.setState(0);
            }
            if (this.state < 0) {
                this.setState(3);
            }
        };
        Shape.prototype.getState = function () {
            return this.state;
        };
        Shape.prototype.setState = function (state) {
            this.state_back = this.state;
            this.state = state;
        };
        Shape.prototype.undoRotate = function () {
            this.state = this.state_back;
        };
        return Shape;
    }());
    Tetris.Shape = Shape;
})(Tetris || (Tetris = {}));
