/// <reference path="../../chalenkoa/www/assets/dep/_ide/jquery.d.ts" />

/// <reference path="AppEventManager.ts" />

module Tetris
{
	export class BtnsTouch
	{
		private context: JQuery;

		private app: App;

		private idIntervalTouch;
		private timeTouchStart;


		constructor (app: App)
		{
			this.app = app;

			this.context = this.app.getContext().find('.btns_touch');

			if ( ! this.isTouchDevice()) return;

			this.context.find('.btn').each((index, btn) =>
			{
				btn.addEventListener(
					"touchstart",
					(e) => {
						e.preventDefault();

						let $btn = $(e.target);
						let keys;

						if ($btn.hasClass('left')) {
							keys = Tetris.AppEventManager.KEY_CODES_GO_LEFT;

						} else if ($btn.hasClass('right')) {
							keys = Tetris.AppEventManager.KEY_CODES_GO_RIGHT;

						} else if ($btn.hasClass('down')) {
							keys = Tetris.AppEventManager.KEY_CODES_GO_DOWN;

						} else if ($btn.hasClass('rotate')) {
							keys = Tetris.AppEventManager.KEY_CODES_ROTATE;
						}

						let key = keys[0];

						jQuery.event.trigger({ type: 'keydown', which: key });

						// click on touch hold
						//--------------------
						if ($btn.hasClass('rotate')) return;

						if (this.idIntervalTouch) {
							clearInterval(this.idIntervalTouch);
						}

						this.timeTouchStart = (new Date()).getTime();

						this.idIntervalTouch = setInterval(
							() => {
								let diff = (new Date()).getTime() - this.timeTouchStart;

								if (diff < 300) return;

								jQuery.event.trigger({ type: 'keydown', which: key });
							},
							1000/30
						);
						//--------------------
					},
					false
				);

				btn.addEventListener(
					"touchend",
					(e) => {
						e.preventDefault();

						let $btn = $(e.target);

						$(window).trigger('keyup');

						if ($btn.hasClass('rotate')) return;

						if (this.idIntervalTouch)
						{
							clearInterval(this.idIntervalTouch);

							this.idIntervalTouch = null;
						}
					}
				);
			});

			this.context.removeClass('hide');
		}


		private  isTouchDevice()
		{
			/** @see https://stackoverflow.com/questions/4817029/whats-the-best-way-to-detect-a-touch-screen-device-using-javascript */
			return 'ontouchstart' in window        // works on most browsers
				|| navigator.maxTouchPoints;       // works on IE10/11 and Surface
		};
	}
}