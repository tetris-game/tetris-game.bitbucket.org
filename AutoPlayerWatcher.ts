
module Tetris
{
	export class AutoPlayerWatcher
	{
		private app;

		private back
		constructor (app: App)
		{
			this.app = app;

			let $modal_msg = this.app.getContext().find('.modal .msg');
			let modal_msg_text_back;

			$($modal_msg).bind("DOMSubtreeModified", () =>
			{
				if ($modal_msg.text() !== modal_msg_text_back && $modal_msg.text().toLowerCase() === 'game over')
				{
					this.gameOver();
				};

				modal_msg_text_back = $modal_msg.text();
			});
		}

		private gameOver()
		{
			let score = $('.toolbar .score .value').text();

			console.log(`game over [score = ${score}]`);

			setTimeout(() =>
			{
				$('.modal button.restart').click();
			}, 0);
		}
	}
}