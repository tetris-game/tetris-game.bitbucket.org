/// <reference path="../../chalenkoa/www/assets/dep/_ide/jquery.d.ts" />

/// <reference path="Shape.ts" />
/// <reference path="NextShape.ts" />
/// <reference path="NextShape.ts" />
/// <reference path="Board.ts" />
/// <reference path="BtnPause.ts" />
/// <reference path="Level.ts" />
/// <reference path="Modal.ts" />
/// <reference path="Audio.ts" />
/// <reference path="BtnsTouch.ts" />
/// <reference path="AppEventManager.ts" />

module Tetris
{
	export enum AppEvent {
		AddShapeOnBoard = 'AppEventAddShapeOnBoard'
	}

	export class App
    {
        public static WIDTH = 10;
        public static HEIGHT = 25;

        private context: JQuery;

        private currentShape: Shape;
        private nextShape: NextShape;
		private board: Tetris.Board;
		private btnPause: Tetris.BtnPause;
		private level: Tetris.Level;
		private modal: Tetris.Modal;
		private audio: Tetris.Audio;
		private btnsTouch: Tetris.BtnsTouch;
		private autoPlayer: Tetris.AutoPlayer;

        private eventManager: Tetris.AppEventManager;

        private timer_main_id;

        private flagStartGame: boolean = false;
        private flagPause: boolean = true;
        private flagGameOver: boolean = false;


        constructor(context:JQuery)
        {
            this.context = context;

            // scale for phone
			if ($(window).width() <= 767) {
				this.context.css({ height: $(window).height() });
			}

            this.eventManager = new Tetris.AppEventManager(this);

            this.board = new Tetris.Board(
            	this.context.find('.board'),
				App.WIDTH,
				App.HEIGHT
			);

			this.nextShape = new Tetris.NextShape(this.context);

			this.btnPause = new Tetris.BtnPause(this);

			this.level = new Tetris.Level(this);

			// Порядок важен
			this.audio = new Tetris.Audio(this);
			this.modal = new Tetris.Modal(this);

			this.btnsTouch = new Tetris.BtnsTouch(this);

			this.context
				.on(Tetris.LevelEvent.UpdateLevel, () =>
				{
					if (this.timer_main_id) {
						clearInterval(this.timer_main_id);
					}

					this.timer_main_id = setInterval(
						this.onApplicationTimerTick.bind(this),
						this.level.getIntervalCurrent()
					);
				})
				.on(Tetris.LevelEvent.UpLevel, () =>
				{
					setTimeout(() => {
						this.audio.play(Tetris.Audio.AUDIO_LEVEL_UP);
					}, 150);
				});

			this.context.trigger(Tetris.LevelEvent.UpdateLevel);

            this.board.getContext()
				.on(Tetris.BoardEvent.CantMove, (e, $atomsCantMove: JQuery[]) =>
				{
					this.audio.play(Tetris.Audio.AUDIO_ATOM_STOP);

					$atomsCantMove.forEach(($atom) =>
					{
						$atom.addClass('stopped');
					});

					setTimeout(
						() =>
						{
							$atomsCantMove.forEach(($atom) =>
							{
								$atom.removeClass('stopped');
							});
						},
						100
					);

					this.removeFullLines();

					this.addShapeOnBoard();
				})
				.on(Tetris.BoardEvent.CantDraw, () =>
				{
					this.gameOver();
				})
				.on(Tetris.BoardEvent.RemoveFullLine, () =>
				{
					this.audio.play(Tetris.Audio.AUDIO_LINE_DELETE);

					this.level.upScore(1)
				});
		}


		public getContext()
		{
			return this.context;
		}

		public getCurrentShape()
		{
			return this.currentShape;
		}

		public getBoard()
		{
			return this.board;
		}

		public getLevel()
		{
			return this.level;
		}

		public getAudio()
		{
			return this.audio;
		}

		public getEventManager()
		{
			return this.eventManager;
		}


		public pause()
		{
			this.btnPause.pause(true);

			this.modal.showPause();

			this.flagPause = true;
		}

		public isPause()
		{
			return this.flagPause;
		}


		public isStartGame()
		{
			return this.flagStartGame;
		}

		public startGame()
		{
			this.flagStartGame = true;
			this.flagGameOver = false;

			this.board.cleanAtoms();

			this.currentShape = null;

			this.nextShape.pull();

			this.level.resetLevel();
			this.level.resetScore();

			this.play();
		}


		public isAutoPlay()
		{
			return !! this.autoPlayer;
		}

		public play(auto_play = null)
		{
			if (auto_play !== null)
			{
				if (auto_play)
				{
					if ( ! this.autoPlayer) {
						this.autoPlayer = new Tetris.AutoPlayer(this);
					}

				} else {
					if (this.autoPlayer) {
						this.autoPlayer.stop();
						this.autoPlayer = null;
					}
				}
			}

			if (this.autoPlayer && this.getCurrentShape()) {
				this.autoPlayer.run();
			}

			if ( ! this.isStartGame()) {
				this.startGame();
			}

			if ( ! this.getCurrentShape()) {
				this.addShapeOnBoard();
			}

			this.btnPause.play(true);

			this.modal.hide();

			this.flagPause = false;
		}


		private onApplicationTimerTick()
        {
        	if (this.flagPause) return;

        	let x = -1;

            let moveResult = this.board.moveShape(
            	this.getCurrentShape(),
				x
			);

            if ( ! moveResult )
            {
				let $atomsCantMove = this.getBoard().getAtomsCantMove(
					this.getCurrentShape(),
					x
				);

				this.board.getContext().trigger(Tetris.BoardEvent.CantMove, [$atomsCantMove]);
            }
        }

        private removeFullLines()
		{
			this.getBoard().removeFullLines();
		}

		private addShapeOnBoard()
		{
			this.currentShape = this.nextShape.pull();

			let board = this.getBoard();

			let rowIdZeroPoint = this.getBoard().getHeight();
			let colIdZeroPoint = this.getBoard().getWidth() / 2 - 1;

			this.currentShape.setRowId(rowIdZeroPoint);
			this.currentShape.setColId(colIdZeroPoint);

			board.drawShape(this.currentShape);

			this.context.trigger(AppEvent.AddShapeOnBoard);
		}


		public isGameOver()
		{
			return this.flagGameOver;
		}

        private gameOver()
        {
            this.pause();

			this.modal.showGameOver();

            this.flagGameOver = true;
        }
    }
}
