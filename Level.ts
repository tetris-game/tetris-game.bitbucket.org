/// <reference path="App.ts" />

module Tetris
{
	export enum LevelEvent {
		UpdateLevel = 'LevelEventUpdateLevel',
		UpLevel = 'LevelEventUpLevel'
	}

	export class Level
	{
		public static INTERVAL_MAX_MS = 1000;
		public static INTERVAL_MIN_MS = 200;

		public static LEVEL_INTERVAL_MIN = 20;

		private app: App;

		private level: number;
		private score: number;


		constructor (app: App) {
			this.app = app;

			this.resetLevel();
			this.resetScore();
		}


		public upLevel()
		{
			this.setLevel(this.getLevel() + 1);

			this.updateScoreTarget();

			this.app.getContext().trigger(Tetris.LevelEvent.UpLevel);
		}

		public resetLevel()
		{
			this.setLevel(1);
		}

		private setLevel(level)
		{
			if (this.level === level) return;

			this.level = level;

			let $level_value = this.app.getContext().find('.level .value');

			$level_value.text(this.getLevel());

			if (this.getLevel() > 1)
			{
				$level_value.addClass('big');

				setTimeout(
					() => {
						$level_value.removeClass('big');
					},
					300
				)
			}

			this.app.getContext().trigger(Tetris.LevelEvent.UpdateLevel);
		}

		public getLevel()
		{
			return this.level;
		}


		public getScore()
		{
			return this.score;
		}

		public upScore(diff = 1)
		{
			this.setScore(this.getScore() + diff);
		}

		public resetScore()
		{
			this.setScore(0);
		}

		private setScore(score)
		{
			if (this.score === score) return;

			this.score = score;

			if (this.score >= this.getScoreTarget()) {
				this.upLevel();
			}

			this.app.getContext().find('.score .value').text(this.getScore());

			this.updateScoreTarget();
		}


		private updateScoreTarget()
		{
			let scoreTargetBack = Tetris.Level.getScoreTargetByLevel(this.getLevel() - 1);
			let scoreTargetCurrent = this.getScoreTarget();

			this.app.getContext().find('.progress_level .value.back').text(scoreTargetBack);
			this.app.getContext().find('.progress_level .value.target').text(scoreTargetCurrent);

			let slide_pct =
				(
					(this.getScore() - scoreTargetBack) /
					(scoreTargetCurrent - scoreTargetBack)
				)
				* 100;

			this.app.getContext().find('.progress_level .slider').width(slide_pct+'%');
		}

		public getScoreTarget()
		{
			return Level.getScoreTargetByLevel(this.getLevel());
		}

		public static getScoreTargetByLevel(level)
		{
			level = parseInt(level);

			if (level < 0) {
				throw new Error('level must be positive');
			};

			if (level === 0) return 0;

			let scoreTargets = {
				1: 5,
				2: 10,
				3: 25,
				4: 50,
				5: 75,
				6: 100,
				7: 150,
				8: 200,
				9: 250,
				10: 300,
				11: 400,
				12: 500,
				13: 650,
				14: 800,
				15: 1000,
				16: 1250,
				17: 1500,
				18: 1750,
				19: 2000,
				20: 2500,
			};

			let target = scoreTargets[level];

			if ( ! target)
			{
				let levels = Object.keys(scoreTargets);

				let levelLast = parseInt( levels[levels.length - 1] );

				let levelBeforeLast = levels[levels.length - 2];

				let diffStep =  scoreTargets[levelLast] - scoreTargets[levelBeforeLast];

				target = scoreTargets[levelLast] + ((level - levelLast) * diffStep)
			}

			return target;
		}


		public getIntervalCurrent()
		{
			let interval;

			if (this.getLevel() > Tetris.Level.LEVEL_INTERVAL_MIN)
			{
				interval = Tetris.Level.INTERVAL_MIN_MS;

			} else {
				// Не линейное изменение скорости от max до min вначале быстрее потом медленее
				let intervalDiff = (Level.INTERVAL_MAX_MS - Tetris.Level.INTERVAL_MIN_MS);
				let exponent = 10;

				interval =
					Level.INTERVAL_MAX_MS -
					(
						intervalDiff - (
							intervalDiff * (
								(
									Math.pow(
										exponent,
										(Tetris.Level.LEVEL_INTERVAL_MIN + 1 - this.getLevel()) / Tetris.Level.LEVEL_INTERVAL_MIN
									) - 1
								) / (exponent - 1)
							)
						)
					);
			}

			return interval;
		}
	}
}