/// <reference path="App.ts" />
var Tetris;
(function (Tetris) {
    var BtnPause = /** @class */ (function () {
        function BtnPause(app) {
            var _this = this;
            this.app = app;
            this.context = this.app.getContext().find('.toolbar .btns button.pause');
            $(window).blur(function () {
                if (_this.isPlay()) {
                    _this.pause();
                }
            });
            this.context.click(function () {
                if (_this.isPlay()) {
                    _this.pause();
                }
                else {
                    _this.play();
                }
            });
            // Убираем фокус с кнопки, чтобы по нажатию пробела не было нажатия на эту кноку
            this.context.on('focus', function () {
                _this.context[0].blur();
            });
        }
        BtnPause.prototype.pause = function (only_state) {
            if (only_state === void 0) { only_state = false; }
            if (this.isPause())
                return;
            this.context.addClass('active');
            if (!only_state) {
                this.app.pause();
            }
        };
        BtnPause.prototype.play = function (only_state) {
            if (only_state === void 0) { only_state = false; }
            if (this.isPlay())
                return;
            this.context.removeClass('active');
            if (!only_state) {
                this.app.play();
            }
        };
        BtnPause.prototype.isPause = function () {
            return this.context.hasClass('active');
        };
        BtnPause.prototype.isPlay = function () {
            return !this.context.hasClass('active');
        };
        return BtnPause;
    }());
    Tetris.BtnPause = BtnPause;
})(Tetris || (Tetris = {}));
