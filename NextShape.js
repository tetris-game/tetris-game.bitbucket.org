/// <reference path="Shape.ts" />
/// <reference path="Board.ts" />
var Tetris;
(function (Tetris) {
    var NextShape = /** @class */ (function () {
        function NextShape($context) {
            this.context = $context.find('.next_shape');
        }
        NextShape.prototype.pull = function () {
            var shape = this.getOrCreate();
            this.shape = null;
            this.getOrCreate();
            return shape;
        };
        NextShape.prototype.getOrCreate = function () {
            if (!this.shape) {
                var shapeTypes = Object.keys(Tetris.ShapeType);
                var shapeTypeRandom = shapeTypes[Math.round(Math.random() * (shapeTypes.length - 1))];
                //shapeTypeRandom = ShapeType.Square;
                var nextShape = new Tetris.Shape(shapeTypeRandom);
                this.set(nextShape);
            }
            return this.shape;
        };
        NextShape.prototype.set = function (shape) {
            this.shape = shape;
            this.update();
        };
        NextShape.prototype.update = function () {
            if (this.board) {
                this.board.delete();
            }
            var board = new Tetris.Board(this.context, 4, 4);
            this.shape.setRowId(4);
            this.shape.setColId(1);
            board.hideBorderEmpty();
            board.drawShape(this.shape);
            board.removeEmptyLines();
            this.board = board;
        };
        return NextShape;
    }());
    Tetris.NextShape = NextShape;
})(Tetris || (Tetris = {}));
