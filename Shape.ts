/// <reference path="ShapeData.ts" />

module Tetris
{
    export enum ShapeType {
        Line = 'Line',
        Square = 'Square',
        LShape = 'LShape',
        JShape = 'JShape',
        Tree = 'Tree',
        ZShape = 'ZShape',
        SShape = 'SShape'
    }

    export class Shape
    {
    	private id;
        private type;

        private data;
        private state;
        private state_back;

        private rowIdCurrent;
        private colIdCurrent;


        constructor(type:ShapeType, rowIdCurrent = 0, colIdCurrent = 0)
        {
        	this.id = Math.random();

            this.type = type;

            this.data = ShapeData.DATA[this.type];

            this.state = 0;
        }

        public getId()
		{
			return this.id;
		}

        public setRowId(rowId)
        {
            this.rowIdCurrent = rowId;
        }

        public getRowId()
        {
            return this.rowIdCurrent;
        }

        public setColId(colId)
        {
            this.colIdCurrent = colId;
        }

        public getColId()
        {
            return this.colIdCurrent;
        }

        public getType() {
            return this.type;
        }

        /**
         * @returns []
         */
        public getData() {
            return this.data[this.state];
        }


        /**
         * Вращение по часовой
         */
        public rotate()
        {
            this._rotate(1);
        }

        /**
         * Вращение против часовой
         */
        public rotateReverse()
        {
			this._rotate(-1);
        }

        private _rotate(state_offset = 0)
		{
			this.setState(this.state + state_offset);

			if (this.state > 3) {
				this.setState(0);
			}

			if (this.state < 0) {
				this.setState(3);
			}
		}

		public getState()
		{
			return this.state;
		}

		private setState(state)
		{
			this.state_back = this.state;

			this.state = state;
		}

		public undoRotate()
		{
			this.state = this.state_back;
		}
    }
}
