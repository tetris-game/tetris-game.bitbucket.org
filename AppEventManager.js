/// <reference path="../../chalenkoa/www/assets/dep/_ide/jquery.d.ts" />
/// <reference path="App.ts" />
var Tetris;
(function (Tetris) {
    var AppEventManager = /** @class */ (function () {
        function AppEventManager(app) {
            var _this = this;
            this.app = app;
            $(window).on('keydown', function (e) {
                if (_this.app.isGameOver())
                    return;
                if (Tetris.AppEventManager.KEY_CODES_PAUSE.indexOf(e.which) > -1) {
                    if (_this.app.isPause()) {
                        _this.app.play();
                    }
                    else {
                        _this.app.pause();
                    }
                }
                if (_this.app.isPause())
                    return;
                if (!_this.idCurrentShape) {
                    _this.idCurrentShape = _this.app.getCurrentShape().getId();
                }
                // предотвращаем срабатывания нажатия нажатого еще для прошлой фигуры
                // для следующей фигуры необходимо отжат кнопку и нажать снова
                if (_this.idCurrentShape !== _this.app.getCurrentShape().getId())
                    return;
                _this.onKeyDown(e);
            });
            $(window).on('keyup', function (e) {
                if (_this.app.isPause())
                    return;
                delete (_this.idCurrentShape);
            });
        }
        AppEventManager.prototype.onKeyDown = function (e) {
            var key_code = e.which;
            var timeNow = (new Date()).getTime() / 1000;
            var fastPress = false;
            if (this.timeLastKeyDown) {
                var intervalSec = (timeNow - this.timeLastKeyDown);
                if (intervalSec < 0.1) {
                    fastPress = true;
                }
            }
            if (AppEventManager.KEY_CODES_ROTATE.indexOf(key_code) > -1) {
                this.onRotate();
            }
            else if (AppEventManager.KEY_CODES_GO_LEFT.indexOf(key_code) > -1) {
                this.onGoLeft();
            }
            else if (AppEventManager.KEY_CODES_GO_RIGHT.indexOf(key_code) > -1) {
                this.onGoRight();
            }
            else if (AppEventManager.KEY_CODES_GO_DOWN.indexOf(key_code) > -1) {
                this.onGoDown(fastPress);
            }
            this.timeLastKeyDown = timeNow;
        };
        AppEventManager.prototype.onRotate = function () {
            var currentShape = this.app.getCurrentShape();
            var board = this.app.getBoard();
            var draw = false;
            board.clearShape(currentShape);
            currentShape.rotate();
            var offsets = [0, 1, -1, 2, -2].forEach(function (offset) {
                if (draw)
                    return;
                if (offset) {
                    currentShape.setColId(currentShape.getColId() + offset);
                }
                if (!board.canDraw(currentShape)) {
                    currentShape.setColId(currentShape.getColId() - offset);
                    return;
                }
                board.drawShape(currentShape);
                draw = true;
            });
            if (!draw) {
                currentShape.rotateReverse();
                board.drawShape(currentShape);
            }
        };
        AppEventManager.prototype.onGoLeft = function () {
            this.app.getBoard().moveShape(this.app.getCurrentShape(), 0, -1);
        };
        AppEventManager.prototype.onGoRight = function () {
            this.app.getBoard().moveShape(this.app.getCurrentShape(), 0, 1);
        };
        AppEventManager.prototype.onGoDown = function (fast) {
            if (fast === void 0) { fast = false; }
            var x = -1;
            // При быстром движении за раз перескакиваем на 2 клетки но нужно убедиться что на одну клетку так же
            // можно перейти так как там может быть какой то препятские
            if (fast
                && this.app.getBoard().canMove(this.app.getCurrentShape(), -2)
                && this.app.getBoard().canMove(this.app.getCurrentShape(), -1)) {
                x = -2;
            }
            var moveResult = this.app.getBoard().moveShape(this.app.getCurrentShape(), x);
            if (!moveResult && Math.abs(x) > 1) {
                x = -1;
                moveResult = this.app.getBoard().moveShape(this.app.getCurrentShape(), x);
            }
            if (!moveResult) {
                var $atomsCantMove = this.app.getBoard().getAtomsCantMove(this.app.getCurrentShape(), x);
                this.app.getBoard().getContext().trigger(Tetris.BoardEvent.CantMove, [$atomsCantMove]);
            }
        };
        AppEventManager.KEY_CODES_PAUSE = [19, 27]; // pause, esc
        AppEventManager.KEY_CODES_GO_LEFT = [37]; // left
        AppEventManager.KEY_CODES_GO_RIGHT = [39]; // right
        AppEventManager.KEY_CODES_GO_DOWN = [40]; // down
        AppEventManager.KEY_CODES_ROTATE = [38, 32]; // up, space
        return AppEventManager;
    }());
    Tetris.AppEventManager = AppEventManager;
})(Tetris || (Tetris = {}));
