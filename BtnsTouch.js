/// <reference path="../../chalenkoa/www/assets/dep/_ide/jquery.d.ts" />
/// <reference path="AppEventManager.ts" />
var Tetris;
(function (Tetris) {
    var BtnsTouch = /** @class */ (function () {
        function BtnsTouch(app) {
            var _this = this;
            this.app = app;
            this.context = this.app.getContext().find('.btns_touch');
            if (!this.isTouchDevice())
                return;
            this.context.find('.btn').each(function (index, btn) {
                btn.addEventListener("touchstart", function (e) {
                    e.preventDefault();
                    var $btn = $(e.target);
                    var keys;
                    if ($btn.hasClass('left')) {
                        keys = Tetris.AppEventManager.KEY_CODES_GO_LEFT;
                    }
                    else if ($btn.hasClass('right')) {
                        keys = Tetris.AppEventManager.KEY_CODES_GO_RIGHT;
                    }
                    else if ($btn.hasClass('down')) {
                        keys = Tetris.AppEventManager.KEY_CODES_GO_DOWN;
                    }
                    else if ($btn.hasClass('rotate')) {
                        keys = Tetris.AppEventManager.KEY_CODES_ROTATE;
                    }
                    var key = keys[0];
                    jQuery.event.trigger({ type: 'keydown', which: key });
                    // click on touch hold
                    //--------------------
                    if ($btn.hasClass('rotate'))
                        return;
                    if (_this.idIntervalTouch) {
                        clearInterval(_this.idIntervalTouch);
                    }
                    _this.timeTouchStart = (new Date()).getTime();
                    _this.idIntervalTouch = setInterval(function () {
                        var diff = (new Date()).getTime() - _this.timeTouchStart;
                        if (diff < 300)
                            return;
                        jQuery.event.trigger({ type: 'keydown', which: key });
                    }, 1000 / 30);
                    //--------------------
                }, false);
                btn.addEventListener("touchend", function (e) {
                    e.preventDefault();
                    var $btn = $(e.target);
                    $(window).trigger('keyup');
                    if ($btn.hasClass('rotate'))
                        return;
                    if (_this.idIntervalTouch) {
                        clearInterval(_this.idIntervalTouch);
                        _this.idIntervalTouch = null;
                    }
                });
            });
            this.context.removeClass('hide');
        }
        BtnsTouch.prototype.isTouchDevice = function () {
            /** @see https://stackoverflow.com/questions/4817029/whats-the-best-way-to-detect-a-touch-screen-device-using-javascript */
            return 'ontouchstart' in window // works on most browsers
                || navigator.maxTouchPoints; // works on IE10/11 and Surface
        };
        ;
        return BtnsTouch;
    }());
    Tetris.BtnsTouch = BtnsTouch;
})(Tetris || (Tetris = {}));
