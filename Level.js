/// <reference path="App.ts" />
var Tetris;
(function (Tetris) {
    var LevelEvent;
    (function (LevelEvent) {
        LevelEvent["UpdateLevel"] = "LevelEventUpdateLevel";
        LevelEvent["UpLevel"] = "LevelEventUpLevel";
    })(LevelEvent = Tetris.LevelEvent || (Tetris.LevelEvent = {}));
    var Level = /** @class */ (function () {
        function Level(app) {
            this.app = app;
            this.resetLevel();
            this.resetScore();
        }
        Level.prototype.upLevel = function () {
            this.setLevel(this.getLevel() + 1);
            this.updateScoreTarget();
            this.app.getContext().trigger(Tetris.LevelEvent.UpLevel);
        };
        Level.prototype.resetLevel = function () {
            this.setLevel(1);
        };
        Level.prototype.setLevel = function (level) {
            if (this.level === level)
                return;
            this.level = level;
            var $level_value = this.app.getContext().find('.level .value');
            $level_value.text(this.getLevel());
            if (this.getLevel() > 1) {
                $level_value.addClass('big');
                setTimeout(function () {
                    $level_value.removeClass('big');
                }, 300);
            }
            this.app.getContext().trigger(Tetris.LevelEvent.UpdateLevel);
        };
        Level.prototype.getLevel = function () {
            return this.level;
        };
        Level.prototype.getScore = function () {
            return this.score;
        };
        Level.prototype.upScore = function (diff) {
            if (diff === void 0) { diff = 1; }
            this.setScore(this.getScore() + diff);
        };
        Level.prototype.resetScore = function () {
            this.setScore(0);
        };
        Level.prototype.setScore = function (score) {
            if (this.score === score)
                return;
            this.score = score;
            if (this.score >= this.getScoreTarget()) {
                this.upLevel();
            }
            this.app.getContext().find('.score .value').text(this.getScore());
            this.updateScoreTarget();
        };
        Level.prototype.updateScoreTarget = function () {
            var scoreTargetBack = Tetris.Level.getScoreTargetByLevel(this.getLevel() - 1);
            var scoreTargetCurrent = this.getScoreTarget();
            this.app.getContext().find('.progress_level .value.back').text(scoreTargetBack);
            this.app.getContext().find('.progress_level .value.target').text(scoreTargetCurrent);
            var slide_pct = ((this.getScore() - scoreTargetBack) /
                (scoreTargetCurrent - scoreTargetBack))
                * 100;
            this.app.getContext().find('.progress_level .slider').width(slide_pct + '%');
        };
        Level.prototype.getScoreTarget = function () {
            return Level.getScoreTargetByLevel(this.getLevel());
        };
        Level.getScoreTargetByLevel = function (level) {
            level = parseInt(level);
            if (level < 0) {
                throw new Error('level must be positive');
            }
            ;
            if (level === 0)
                return 0;
            var scoreTargets = {
                1: 5,
                2: 10,
                3: 25,
                4: 50,
                5: 75,
                6: 100,
                7: 150,
                8: 200,
                9: 250,
                10: 300,
                11: 400,
                12: 500,
                13: 650,
                14: 800,
                15: 1000,
                16: 1250,
                17: 1500,
                18: 1750,
                19: 2000,
                20: 2500,
            };
            var target = scoreTargets[level];
            if (!target) {
                var levels = Object.keys(scoreTargets);
                var levelLast = parseInt(levels[levels.length - 1]);
                var levelBeforeLast = levels[levels.length - 2];
                var diffStep = scoreTargets[levelLast] - scoreTargets[levelBeforeLast];
                target = scoreTargets[levelLast] + ((level - levelLast) * diffStep);
            }
            return target;
        };
        Level.prototype.getIntervalCurrent = function () {
            var interval;
            if (this.getLevel() > Tetris.Level.LEVEL_INTERVAL_MIN) {
                interval = Tetris.Level.INTERVAL_MIN_MS;
            }
            else {
                // Не линейное изменение скорости от max до min вначале быстрее потом медленее
                var intervalDiff = (Level.INTERVAL_MAX_MS - Tetris.Level.INTERVAL_MIN_MS);
                var exponent = 10;
                interval =
                    Level.INTERVAL_MAX_MS -
                        (intervalDiff - (intervalDiff * ((Math.pow(exponent, (Tetris.Level.LEVEL_INTERVAL_MIN + 1 - this.getLevel()) / Tetris.Level.LEVEL_INTERVAL_MIN) - 1) / (exponent - 1))));
            }
            return interval;
        };
        Level.INTERVAL_MAX_MS = 1000;
        Level.INTERVAL_MIN_MS = 200;
        Level.LEVEL_INTERVAL_MIN = 20;
        return Level;
    }());
    Tetris.Level = Level;
})(Tetris || (Tetris = {}));
