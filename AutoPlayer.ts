
/// <reference path="App.ts" />

module Tetris
{
	interface Variant {
		shapeState: number,
		colIdBoard: number,
		rowIdBoard: number,
		emptyAtom: number,
		emptyAtomColIds: {}
	}

	export class AutoPlayer
	{
		private app: App;


		constructor (app: App)
		{
			this.app = app;

			this.app.getContext().on(AppEvent.AddShapeOnBoard, () => {
				this.run();
			});

			let auto_player_watcher = this.app.getContext().data('auto_player_watcher');
			if ( ! auto_player_watcher) {
				this.app.getContext().data('auto_player_watcher', new Tetris.AutoPlayerWatcher(this.app));
			}

		}

		public stop()
		{
			this.app.getContext().off(AppEvent.AddShapeOnBoard);
		}

		public run()
		{
			let variants: Variant[] = [];

			// Получаем карту поверхности на доске
			let mapUpBord = this.getRowIsUpForCol();

			// Детектим дыры
			//--------------
			const minDepthHole = 3;

			let colIdHole = null;
			let minRowIdHole = null;

			mapUpBord.forEach((rowId, colId) =>
			{
				if (
						(
								colId === 1
							&& 	(mapUpBord[colId + 1] - rowId) >= minDepthHole
						)
					||	(
								(mapUpBord[colId + 1] - rowId) >= minDepthHole
							&&	(mapUpBord[colId - 1] - rowId) >= minDepthHole
						)
					||	(
								colId === this.app.getBoard().getWidth()
							&& 	(mapUpBord[colId - 1] - rowId) >= minDepthHole
						)
				) {
					if (
							! colIdHole
						||	minRowIdHole > rowId
					) {
						colIdHole = colId;
						minRowIdHole = rowId;
					}
				}
			});
			//--------------

			[1,2,3,4].forEach(() =>
			{
				this.app.getCurrentShape().rotate();

				// Получаем карту дна текущей фигуры
				//----------------------------------
				/** @see https://stackoverflow.com/questions/7848004/get-column-from-a-two-dimensional-array-in-javascript */
				const arrayColumn = (arr, n) => arr.map(x => x[n]);

				let shape = this.app.getCurrentShape();
				let shapeData = shape.getData();

				let mapDownShape = [];
				for (let colId = 0; colId < 4; colId ++) {
					let rowIds = arrayColumn(shapeData, colId);
					let rowId = rowIds.lastIndexOf(1);

					mapDownShape[colId] = rowId;
				}
				//----------------------------------

				// Ищем совпадения
				//----------------
				let offsetColId = mapDownShape.findIndex((colId) =>
				{
					return colId > -1
				});

				let mapDownShapeOnlyShape = mapDownShape.filter((colId) =>
				{
					return colId === -1 ? false : true;
				});

				mapUpBord.forEach((rowIdBoard, colIdBoard, mapUpBord) =>
				{
					let sumColIds = mapDownShapeOnlyShape.map((rowIdShape, colIdShape) =>
					{
						return rowIdShape + mapUpBord[colIdShape+colIdBoard];
					});

					/** @see https://stackoverflow.com/questions/1669190/find-the-min-max-element-of-an-array-in-javascript */
					let maxSum = Math.max.apply(null, sumColIds);

					let diffsSumColIds = sumColIds.map((sum) =>
					{
						return Math.abs(sum - maxSum);
					});

					let emptyAtom = diffsSumColIds.reduce((a, b) => {
						return a + b;
					});

					if (isNaN(emptyAtom)) return;

					let emptyAtomColIds = {};

					diffsSumColIds.forEach((emptyAtom, colId) =>
					{
						emptyAtomColIds[colId + colIdBoard] = emptyAtom;
					})

					variants.push({
						shapeState: shape.getState(),
						colIdBoard: colIdBoard - offsetColId,
						rowIdBoard: rowIdBoard,
						emptyAtom:  emptyAtom,
						emptyAtomColIds: emptyAtomColIds
					});
				});
			});


			if (colIdHole) {
				variants = variants.filter((variant: Variant) =>
				{
					return (
						Object.keys(variant.emptyAtomColIds).indexOf(''+colIdHole) > -1
						? true
						: false
					);
				});

			}

			variants.sort((variant_a: Variant, variant_b: Variant) =>
			{
				const weightEmptyAtom = 100;
				const weightRowId = 1;

				let a = (variant_a.emptyAtom * weightEmptyAtom) + (variant_a.rowIdBoard * weightRowId);
				let b = (variant_b.emptyAtom * weightEmptyAtom) + (variant_b.rowIdBoard * weightRowId);

				if (a < b ) {
					return -1;
				}
				if (a > b) {
					return 1;
				}
				return 0;
			});

			let variantBest = variants[0];

			this.applyVariant(variantBest);
		}

		private applyVariant(variant: Variant)
		{
			setTimeout(() =>
			{
				let shape = this.app.getCurrentShape();

				[1,2,3,4].forEach(() =>
				{
					if (variant.shapeState === shape.getState()) return;

					jQuery.event.trigger({ type: 'keydown', which: Tetris.AppEventManager.KEY_CODES_ROTATE[0] });
				});


				let diffColId = variant.colIdBoard - shape.getColId();

				let moveDown = () =>
				{
					let idIntervalDown = setInterval(() =>
					{
						if (this.app.isPause()) {
							clearInterval(idIntervalDown);
							return;
						}

						let canMove = this.app.getBoard().canMove(
							shape,
							-1
						)

						if (canMove) {
							jQuery.event.trigger({ type: 'keydown', which: Tetris.AppEventManager.KEY_CODES_GO_DOWN[0] });
						}

						if ( ! canMove)
						{
							clearInterval(idIntervalDown);

							jQuery.event.trigger({ type: 'keyup' });

							return;
						}

					}, 1000 / 100);

				};

				if (diffColId === 0) {
					moveDown();

				} else {
					let which = (
						( diffColId < 0 )
						? Tetris.AppEventManager.KEY_CODES_GO_LEFT[0]
						: Tetris.AppEventManager.KEY_CODES_GO_RIGHT[0]
					);

					let counter = 0;

					let idIntervalSide = setInterval(() =>
					{
						if (counter >= Math.abs(diffColId))
						{
							clearInterval(idIntervalSide);

							jQuery.event.trigger({ type: 'keyup' });

							moveDown();

							return;
						}

						counter ++;

						jQuery.event.trigger({ type: 'keydown', which: which });

					}, 1000 / 30);
				}

			}, 500);
		}

		private getRowIsUpForCol()
		{
			// [col_id] => [row_id_1, row_id_2, ...]
			let rowIdsForCol = [];
			let atomsAdded = [];

			for(let colId = 1; colId <= this.app.getBoard().getWidth(); colId ++)
			{
				rowIdsForCol[colId] = [0];
			}

			let board = this.app.getBoard();

			let $atomsFirstRow: JQuery = $('.board .atoms li:last .atom.shape');

			if ($atomsFirstRow.length > 0)
			{
				let addAtoms = (atoms: JQuery[]) =>
				{
					atoms = atoms.filter(($atom) =>
					{
						if (atomsAdded.indexOf($atom[0]) > -1) return false;

						atomsAdded.push($atom[0]);

						return true;
					});

					atoms.forEach(($atom:JQuery) =>
					{
						let colId = board.getColIdForAtom($atom);
						let rowId = board.getRowIdForAtom($atom);

						if ( ! rowIdsForCol[colId]) {
							rowIdsForCol[colId] = [];
						}
						if (rowIdsForCol[colId].indexOf(rowId) === -1) {
							rowIdsForCol[colId].push(rowId);
						}
					});

					return atoms;
				};

				let addAtomsForEach = (atoms) =>
				{
					atoms = addAtoms(atoms);

					if (atoms.length === 0) return;

					atoms.forEach(($atom:JQuery) =>
					{
						atoms = this.getAtomsShapeAroundAtom($atom);

						addAtomsForEach(atoms);
					});
				};

				$atomsFirstRow.each((index, elem) => {
					addAtomsForEach([$(elem)]);
				});
			}

			for (let colId in rowIdsForCol)
			{
				let rowIds = rowIdsForCol[colId];
				let rowIdMax = Math.max.apply(null, rowIds);

				rowIdsForCol[colId] = rowIdMax;
			}

			return rowIdsForCol;
		}

		private getAtomsShapeAroundAtom($atom:JQuery)
		{
			let board = this.app.getBoard();

			let rowId = board.getRowIdForAtom($atom);
			let cowId = board.getColIdForAtom($atom);

			let atoms = [];

			atoms.push(board.getAtom(rowId, cowId - 1));
			atoms.push(board.getAtom(rowId + 1, cowId - 1));
			atoms.push(board.getAtom(rowId + 1, cowId));
			atoms.push(board.getAtom(rowId + 1, cowId + 1));
			atoms.push(board.getAtom(rowId, cowId + 1));

			atoms = atoms.filter(($atom:JQuery) => {
				return $atom.hasClass('shape') ? true : false;
			})

			return atoms;
		}
	}
}