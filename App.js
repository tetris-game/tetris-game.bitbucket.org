/// <reference path="../../chalenkoa/www/assets/dep/_ide/jquery.d.ts" />
/// <reference path="Shape.ts" />
/// <reference path="NextShape.ts" />
/// <reference path="NextShape.ts" />
/// <reference path="Board.ts" />
/// <reference path="BtnPause.ts" />
/// <reference path="Level.ts" />
/// <reference path="Modal.ts" />
/// <reference path="Audio.ts" />
/// <reference path="BtnsTouch.ts" />
/// <reference path="AppEventManager.ts" />
var Tetris;
(function (Tetris) {
    var AppEvent;
    (function (AppEvent) {
        AppEvent["AddShapeOnBoard"] = "AppEventAddShapeOnBoard";
    })(AppEvent = Tetris.AppEvent || (Tetris.AppEvent = {}));
    var App = /** @class */ (function () {
        function App(context) {
            var _this = this;
            this.flagStartGame = false;
            this.flagPause = true;
            this.flagGameOver = false;
            this.context = context;
            // scale for phone
            if ($(window).width() <= 767) {
                this.context.css({ height: $(window).height() });
            }
            this.eventManager = new Tetris.AppEventManager(this);
            this.board = new Tetris.Board(this.context.find('.board'), App.WIDTH, App.HEIGHT);
            this.nextShape = new Tetris.NextShape(this.context);
            this.btnPause = new Tetris.BtnPause(this);
            this.level = new Tetris.Level(this);
            // Порядок важен
            this.audio = new Tetris.Audio(this);
            this.modal = new Tetris.Modal(this);
            this.btnsTouch = new Tetris.BtnsTouch(this);
            this.context
                .on(Tetris.LevelEvent.UpdateLevel, function () {
                if (_this.timer_main_id) {
                    clearInterval(_this.timer_main_id);
                }
                _this.timer_main_id = setInterval(_this.onApplicationTimerTick.bind(_this), _this.level.getIntervalCurrent());
            })
                .on(Tetris.LevelEvent.UpLevel, function () {
                setTimeout(function () {
                    _this.audio.play(Tetris.Audio.AUDIO_LEVEL_UP);
                }, 150);
            });
            this.context.trigger(Tetris.LevelEvent.UpdateLevel);
            this.board.getContext()
                .on(Tetris.BoardEvent.CantMove, function (e, $atomsCantMove) {
                _this.audio.play(Tetris.Audio.AUDIO_ATOM_STOP);
                $atomsCantMove.forEach(function ($atom) {
                    $atom.addClass('stopped');
                });
                setTimeout(function () {
                    $atomsCantMove.forEach(function ($atom) {
                        $atom.removeClass('stopped');
                    });
                }, 100);
                _this.removeFullLines();
                _this.addShapeOnBoard();
            })
                .on(Tetris.BoardEvent.CantDraw, function () {
                _this.gameOver();
            })
                .on(Tetris.BoardEvent.RemoveFullLine, function () {
                _this.audio.play(Tetris.Audio.AUDIO_LINE_DELETE);
                _this.level.upScore(1);
            });
        }
        App.prototype.getContext = function () {
            return this.context;
        };
        App.prototype.getCurrentShape = function () {
            return this.currentShape;
        };
        App.prototype.getBoard = function () {
            return this.board;
        };
        App.prototype.getLevel = function () {
            return this.level;
        };
        App.prototype.getAudio = function () {
            return this.audio;
        };
        App.prototype.getEventManager = function () {
            return this.eventManager;
        };
        App.prototype.pause = function () {
            this.btnPause.pause(true);
            this.modal.showPause();
            this.flagPause = true;
        };
        App.prototype.isPause = function () {
            return this.flagPause;
        };
        App.prototype.isStartGame = function () {
            return this.flagStartGame;
        };
        App.prototype.startGame = function () {
            this.flagStartGame = true;
            this.flagGameOver = false;
            this.board.cleanAtoms();
            this.currentShape = null;
            this.nextShape.pull();
            this.level.resetLevel();
            this.level.resetScore();
            this.play();
        };
        App.prototype.isAutoPlay = function () {
            return !!this.autoPlayer;
        };
        App.prototype.play = function (auto_play) {
            if (auto_play === void 0) { auto_play = null; }
            if (auto_play !== null) {
                if (auto_play) {
                    if (!this.autoPlayer) {
                        this.autoPlayer = new Tetris.AutoPlayer(this);
                    }
                }
                else {
                    if (this.autoPlayer) {
                        this.autoPlayer.stop();
                        this.autoPlayer = null;
                    }
                }
            }
            if (this.autoPlayer && this.getCurrentShape()) {
                this.autoPlayer.run();
            }
            if (!this.isStartGame()) {
                this.startGame();
            }
            if (!this.getCurrentShape()) {
                this.addShapeOnBoard();
            }
            this.btnPause.play(true);
            this.modal.hide();
            this.flagPause = false;
        };
        App.prototype.onApplicationTimerTick = function () {
            if (this.flagPause)
                return;
            var x = -1;
            var moveResult = this.board.moveShape(this.getCurrentShape(), x);
            if (!moveResult) {
                var $atomsCantMove = this.getBoard().getAtomsCantMove(this.getCurrentShape(), x);
                this.board.getContext().trigger(Tetris.BoardEvent.CantMove, [$atomsCantMove]);
            }
        };
        App.prototype.removeFullLines = function () {
            this.getBoard().removeFullLines();
        };
        App.prototype.addShapeOnBoard = function () {
            this.currentShape = this.nextShape.pull();
            var board = this.getBoard();
            var rowIdZeroPoint = this.getBoard().getHeight();
            var colIdZeroPoint = this.getBoard().getWidth() / 2 - 1;
            this.currentShape.setRowId(rowIdZeroPoint);
            this.currentShape.setColId(colIdZeroPoint);
            board.drawShape(this.currentShape);
            this.context.trigger(AppEvent.AddShapeOnBoard);
        };
        App.prototype.isGameOver = function () {
            return this.flagGameOver;
        };
        App.prototype.gameOver = function () {
            this.pause();
            this.modal.showGameOver();
            this.flagGameOver = true;
        };
        App.WIDTH = 10;
        App.HEIGHT = 25;
        return App;
    }());
    Tetris.App = App;
})(Tetris || (Tetris = {}));
