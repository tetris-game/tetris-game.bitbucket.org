/// <reference path="Shape.ts" />
/// <reference path="Board.ts" />

module Tetris
{
    export class NextShape
    {
		private context;

        private shape:Shape;
       	private board:Board;


        constructor ($context:JQuery)
        {
            this.context = $context.find('.next_shape');
        }


		public pull():Shape
		{
			let shape = this.getOrCreate();

			this.shape = null;

			this.getOrCreate();

			return shape;
		}


        private getOrCreate():Shape
        {
            if ( ! this.shape)
            {
                let shapeTypes:any = Object.keys(Tetris.ShapeType);

                let shapeTypeRandom:ShapeType = shapeTypes[Math.round(Math.random() * (shapeTypes.length - 1))];

				//shapeTypeRandom = ShapeType.Square;

                let nextShape = new Shape(shapeTypeRandom);

                this.set(nextShape);
            }
            return this.shape;
        }

        private set(shape)
        {
            this.shape = shape;

            this.update();
        }

        private update()
		{
			if (this.board) {
				this.board.delete();
			}

			let board = new Tetris.Board(
				this.context,
				4,
				4
			);

			this.shape.setRowId(4);
			this.shape.setColId(1);

			board.hideBorderEmpty();

			board.drawShape(this.shape);

			board.removeEmptyLines();

			this.board = board;
        }
    }
}
